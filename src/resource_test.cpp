#include "DxLib.h"

#include "Resource.h"

#include <iostream>
#include <string>

static void testResource() {

	std::string resource_list = "data/rclist.csv";
	//std::string resource_list = "data/rclist_win.csv";
	kdna::Resource rc(resource_list);

	// 初期化の確認
	std::cout << "isEmpty -- " << ((rc.isEmpty()) ? "TRUE" : "FALSE") << std::endl;
	std::cout << "----------------------------------------" << std::endl;

	// 通常のロード
	rc.load("t001");
	std::cout << "load(\"t001\")" << std::endl;
	std::cout << "isEmpty -- " << ((rc.isEmpty()) ? "TRUE" : "FALSE") << std::endl;

	std::cout << "t001:img:da:Actor4 -- " << rc.getResource("t001:img:da:Actor4") << std::endl;
	std::cout << "t001:bgm:da:sample_bgm1 -- " << rc.getResource("t001:bgm:da:sample_bgm1") << std::endl;
	std::cout << "t001:se:da:sample_se1 -- " << rc.getResource("t001:se:da:sample_se1") << std::endl;

	std::cout << "t001:img:da:Actor4 -- " << rc["t001:img:da:Actor4"] << std::endl;
	std::cout << "t001:bgm:da:sample_bgm1 -- " << rc["t001:bgm:da:sample_bgm1"] << std::endl;
	std::cout << "t001:se:da:sample_se1 -- " << rc["t001:se:da:sample_se1"] << std::endl;
	std::cout << "----------------------------------------" << std::endl;

	// 以下はコンパイル時にエラー
	//rc["t001:img:da:Actor4"] = 111111;

	// 既にロードしたタスクをもう一度ロードする
	// assert発生
	//rc.load("t001");

	// 存在しないキー値にアクセス
	std::cout << "存在しないキー値にアクセス" << std::endl;
	std::cout << "hogehoge -- " << rc.getResource("hogehoge") << std::endl;
	std::cout << "hogehoge -- " << rc["hogehoge"] << std::endl;
	std::cout << "----------------------------------------" << std::endl;

	// 削除したキー値にアクセス
	rc.release("t001:img:da:Actor4");
	std::cout << "release(\"t001:img:da:Actor4\")" << std::endl;
	std::cout << "isEmpty -- " << ((rc.isEmpty()) ? "TRUE" : "FALSE") << std::endl;
	std::cout << "isEmpty(\"t001:img:da:Actor4\") -- " << ((rc.isEmpty("t001:img:da:Actor4")) ? "TRUE" : "FALSE") << std::endl;
	std::cout << "t001:img:da:Actor4 -- " << rc.getResource("t001:img:da:Actor4") << std::endl;
	std::cout << "t001:img:da:Actor4 -- " << rc["t001:img:da:Actor4"] << std::endl;
	std::cout << "t001:bgm:da:sample_bgm1 -- " << rc.getResource("t001:bgm:da:sample_bgm1") << std::endl;
	std::cout << "t001:bgm:da:sample_bgm1 -- " << rc["t001:bgm:da:sample_bgm1"] << std::endl;
	std::cout << "----------------------------------------" << std::endl;

	// setResourceで新たにロード
	rc.setResource("t001", "img", "data/Actor4.png");
	std::cout << "setResource(\"t001\", \"img\", \"data/Actor4.png\")" << std::endl;
	std::cout << "isEmpty -- " << ((rc.isEmpty()) ? "TRUE" : "FALSE") << std::endl;
	std::cout << "isEmpty(\"t001:img:da:Actor4\") -- " << ((rc.isEmpty("t001:img:da:Actor4")) ? "TRUE" : "FALSE") << std::endl;
	std::cout << "t001:img:da:Actor4 -- " << rc.getResource("t001:img:da:Actor4") << std::endl;
	std::cout << "t001:img:da:Actor4 -- " << rc["t001:img:da:Actor4"] << std::endl;
	std::cout << "t001:bgm:da:sample_bgm1 -- " << rc.getResource("t001:bgm:da:sample_bgm1") << std::endl;
	std::cout << "t001:bgm:da:sample_bgm1 -- " << rc["t001:bgm:da:sample_bgm1"] << std::endl;
	std::cout << "----------------------------------------" << std::endl;

	// releaseAllで全削除
	rc.releaseAll();
	std::cout << "releaseAll()" << std::endl;
	std::cout << "isEmpty -- " << ((rc.isEmpty()) ? "TRUE" : "FALSE") << std::endl;
	std::cout << "isEmpty(\"t001:img:da:Actor4\") -- " << ((rc.isEmpty("t001:img:da:Actor4")) ? "TRUE" : "FALSE") << std::endl;
	std::cout << "isEmpty(\"t001:bgm:da:sample_bgm1\") -- " << ((rc.isEmpty("t001:bgm:da:sample_bgm1")) ? "TRUE" : "FALSE") << std::endl;
	std::cout << "isEmpty(\"t001:se:da:sample_se1\") -- " << ((rc.isEmpty("da:sample_se=")) ? "TRUE" : "FALSE") << std::endl;
	std::cout << "t001:img:da:Actor4 -- " << rc.getResource("t001:img:da:Actor4") << std::endl;
	std::cout << "t001:bgm:da:sample_bgm1 -- " << rc.getResource("t001:bgm:da:sample_bgm1") << std::endl;
	std::cout << "t001:se:da:sample_se1 -- " << rc.getResource("t001:se:da:sample_se1") << std::endl;
	std::cout << "----------------------------------------" << std::endl;
}

static void testImageDraw() {

	// リソースのロード
	std::string resource_list = "data/rclist.csv";
	kdna::Resource rc(resource_list.c_str());
	std::cout << "##### testDraw #####" << std::endl;
	rc.load("t001");

	// ハンドルの取得パラメータの設定
	int ghandle = rc.getResource("t001:img:da:Actor4");
	int width = 0;
	int height = 0;
	GetGraphSize(ghandle, &width, &height);
	int chipwidth = width / 3;
	int chipheight = height / 4;

	// 描画その１
	int result = DrawGraph(0, 0, ghandle, false);
	std::cout << "DrawResult " << result << std::endl;

	// 描画その２
	std::pair<int, int> chiplocation = {0, 3};
	int destx = width + chipwidth;
	int desty = 0;
	int srcx = chiplocation.first * chipwidth;
	int srcy = chiplocation.second * chipheight;
	result = DrawRectGraph(destx, desty, srcx, srcy, chipwidth, chipheight, ghandle, true, false);
	std::cout << "DrawResult " << result << std::endl;

	// 画像を開放 描画には失敗する
	rc.release("t001:img:da:Actor4");
	result = DrawGraph(0, height+10, ghandle, false);
	std::cout << "DrawResult " << result << std::endl;
	std::cout << "----------------------------------------" << std::endl;

	// 画像を再ロード 前のハンドルでは描画に失敗し，新しいハンドルでは成功する
	rc.setResource("t001", "img", "data/Actor4.png");
	int new_ghandle = rc.getResource("t001:img:da:Actor4");
	result = DrawGraph(0, height+10, ghandle, false);
	std::cout << "DrawResult(old handle) " << result << std::endl;
	result = DrawGraph(width+10, height+10, new_ghandle, false);
	std::cout << "DrawResult(new handle) " << result << std::endl;
	std::cout << "----------------------------------------" << std::endl;

	rc.releaseAll();
}

static void testSoundPlay() {

	// リソースのロード
	std::string resource_list = "data/rclist.csv";
	kdna::Resource rc(resource_list.c_str());
	std::cout << "##### testPlay #####" << std::endl;
	rc.load("t001");

	// ハンドルの取得
	int bgm_handle = rc.getResource("t001:bgm:da:sample_bgm1");
	int se_handle = rc.getResource("t001:se:da:sample_se1");

	// bgmを鳴らす
	int wait = 15 * 1000;	// 15秒
	ChangeVolumeSoundMem(255, bgm_handle);
	int result = PlaySoundMem(bgm_handle, DX_PLAYTYPE_BACK, true);
	std::cout << "PlayResult(BGM) " << result << std::endl;
	std::cout << "----------------------------------------" << std::endl;
	WaitTimer(wait);
	StopSoundMem(bgm_handle);

	// seを鳴らす
	ChangeVolumeSoundMem(255, se_handle);
	result = PlaySoundMem(se_handle, DX_PLAYTYPE_BACK, true);
	std::cout << "PlayResult(SE) " << result << std::endl;
	std::cout << "----------------------------------------" << std::endl;
	while (CheckSoundMem(se_handle) == 1) {}

	// seを連続で再生する（重なっている？）
	ChangeVolumeSoundMem(255, se_handle);
	result = PlaySoundMem(se_handle, DX_PLAYTYPE_BACK, true);
	std::cout << "PlayResult(SE)(1) " << result << std::endl;
	std::cout << "----------------------------------------" << std::endl;
	result = PlaySoundMem(se_handle, DX_PLAYTYPE_BACK, true);
	std::cout << "PlayResult(SE)(2) " << result << std::endl;
	std::cout << "----------------------------------------" << std::endl;
	result = PlaySoundMem(se_handle, DX_PLAYTYPE_BACK, true);
	std::cout << "PlayResult(SE)(3) " << result << std::endl;
	std::cout << "----------------------------------------" << std::endl;
	result = PlaySoundMem(se_handle, DX_PLAYTYPE_BACK, true);
	std::cout << "PlayResult(SE)(4) " << result << std::endl;
	std::cout << "----------------------------------------" << std::endl;
	WaitKey();	// 入力待ち

	// 音声を開放後，再生（再生に失敗する）
	rc.release("t001:bgm:da:sample_bgm1");
	rc.release("t001:se:da:sample_se1");
	// bgmを鳴らす
	wait = 5 * 1000;	// 5秒
	ChangeVolumeSoundMem(255, bgm_handle);
	result = PlaySoundMem(bgm_handle, DX_PLAYTYPE_BACK, true);
	std::cout << "PlayResult(BGM) " << result << std::endl;
	std::cout << "----------------------------------------" << std::endl;
	WaitTimer(wait);
	StopSoundMem(bgm_handle);
	// seを鳴らす
	ChangeVolumeSoundMem(255, se_handle);
	result = PlaySoundMem(se_handle, DX_PLAYTYPE_BACK, true);
	std::cout << "PlayResult(SE) " << result << std::endl;
	std::cout << "----------------------------------------" << std::endl;
	while (CheckSoundMem(se_handle) == 1) {}

	// 再ロード後，もう一度鳴らす
	rc.setResource("t001", "bgm", "data/sample_bgm1.mp3");
	rc.setResource("t001", "se", "data/sample_se1.wav");
	// ハンドルの再取得
	int new_bgm_handle = rc.getResource("t001:bgm:da:sample_bgm1");
	int new_se_handle = rc.getResource("t001:se:da:sample_se1");
	// seを鳴らす
	ChangeVolumeSoundMem(255, se_handle);
	result = PlaySoundMem(se_handle, DX_PLAYTYPE_BACK, true);
	std::cout << "PlayResult(old_SE) " << result << std::endl;
	std::cout << "----------------------------------------" << std::endl;
	WaitKey();	//入力待ち
	ChangeVolumeSoundMem(255, new_se_handle);
	result = PlaySoundMem(new_se_handle, DX_PLAYTYPE_BACK, true);
	std::cout << "PlayResult(new_SE) " << result << std::endl;
	std::cout << "----------------------------------------" << std::endl;
	WaitKey();	//入力待ち
	// bgmを鳴らす
	ChangeVolumeSoundMem(255, bgm_handle);
	result = PlaySoundMem(bgm_handle, DX_PLAYTYPE_BACK, true);
	std::cout << "PlayResult(old_BGM) " << result << std::endl;
	std::cout << "----------------------------------------" << std::endl;
	WaitKey();	//入力待ち
	ChangeVolumeSoundMem(255, new_bgm_handle);
	result = PlaySoundMem(new_bgm_handle, DX_PLAYTYPE_BACK, true);
	std::cout << "PlayResult(new_BGM) " << result << std::endl;
	std::cout << "----------------------------------------" << std::endl;
	WaitKey();	//入力待ち
	StopSoundMem(bgm_handle);

	rc.releaseAll();
}
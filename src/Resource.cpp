/*! @file
 @brief 画像ファイル，音声ファイルといったリソースをロードし管理するクラス
 @author 金子
 @date 作成日(2014/10/18)
 */

#include "Resource.h"
#include "CSVReader.h"

#include "DxLib.h"

#include <iostream>
#include <string>
#include <unordered_map>
#include <regex>

#include <cassert>

// 名前空間の明記
using kdna::ResourceHandle;
using kdna::ImageHandle;
using kdna::BGMHandle;
using kdna::SEHandle;
using kdna::ResourceHandleFactory;
using kdna::ResourceListReader;
using kdna::ResourceHash;
using kdna::Resource;

//------------------------------------------------------------------------------------------------------
//  ImageHandleの実装
//------------------------------------------------------------------------------------------------------

/*! @brief デストラクタ
 m_handleを使用してメモリ上にロードした画像を開放する
 */
ImageHandle::~ImageHandle() {
	if (DeleteGraph(getHandle()) == -1) {
		assert("画像の開放に失敗しました\n");
		exit(1);
	}
}


//------------------------------------------------------------------------------------------------------
//  BGMHandleの実装
//------------------------------------------------------------------------------------------------------

/*! @brief デストラクタ
 m_handleを使用してメモリ上にロードした音声を開放する
 */
BGMHandle::~BGMHandle() {
	if (DeleteSoundMem(getHandle()) == -1) {
		assert("音声の開放に失敗しました\n");
		exit(1);
	}
}


//------------------------------------------------------------------------------------------------------
//  BGMHandleの実装
//------------------------------------------------------------------------------------------------------

/*! @brief デストラクタ
 m_handleを使用してメモリ上にロードした音声を開放する
 */
SEHandle::~SEHandle() {
	if (DeleteSoundMem(getHandle()) == -1) {
		assert("音声の開放に失敗しました\n");
		exit(1);
	}
}

//------------------------------------------------------------------------------------------------------
//  ResourceHandleFactoryの実装
//------------------------------------------------------------------------------------------------------

/*! @brief ResourceHandleのサブクラスのインスタンスを生成する．
 それぞれのサブクラスに登録するハンドルを取得するため，リソースのロードもこのメソッドが行う
 param[in] filename ロードするリソースファイル名
 param[in] resourcetype ロードするリソースの種類
 return ResourceHandleのサブクラスのオブジェクトのポインタ，生成に失敗した場合はnullptrを返す
 */
ResourceHandle* ResourceHandleFactory::createHandle(std::string filename, std::string resourcetype) {
	if (resourcetype == "img") {
		int handle = LoadGraph(filename.c_str());
		if (handle == -1) { return nullptr; }
		return new ImageHandle(handle);
	}
	if (resourcetype == "bgm") {
		int handle = LoadSoundMem(filename.c_str());
		if (handle == -1) { return nullptr; }
		return new ImageHandle(handle);
	}
	if (resourcetype == "se") {
		int handle = LoadSoundMem(filename.c_str());
		if (handle == -1) { return nullptr; }
		return new ImageHandle(handle);
	}

	return nullptr;
}


//------------------------------------------------------------------------------------------------------
//  ResourceListReaderの実装
//------------------------------------------------------------------------------------------------------

/*! @brief コンストラクタ
 csvファイルを読み込んで，データを整形し，m_formatted_dataに登録する
 param[in] filename csvファイルのパス
 */
ResourceListReader::ResourceListReader(const char* filename) {
	// csvファイルを読み込む
	CSVReader csv;
	if( !csv.load(filename)) { assert("csvファイルの読み込みに失敗"); }
	assert(csv.getRow() > 1 && "csvファイルが一行以下");

	m_head.first = "";
	m_head.second = 0;

	std::vector<std::string> buffer;
	buffer = csv.readLine();	// 1行目を捨てる
	for(buffer = csv.readLine(); buffer.size() > 0; buffer = csv.readLine()) {
		ResourceProperty rp;
		rp.task_id = buffer[0];
		rp.resourcetype = buffer[1];
		rp.filename = buffer[2];

		m_formatted_data.push_back(rp);
	}
}

/*! @brief task_idでresourcetypeとfilenameを検索する
 同じtask_idで検索する場合は，前に検索した行よりも先を探索する．
 param[in] task_id タスクに振り分けられたID
 return resourcetypeとfilenameのペアを返す. 見つからなかった場合は空のpairを返す
 */
std::pair<std::string, std::string> ResourceListReader::search(std::string task_id) {
	// 前回の検索とtask_idが異なる場合m_headを初期化
	if(task_id != m_head.first) {
		m_head.first = task_id;
		m_head.second = 0;
	}
	else if(m_head.second == m_formatted_data.size()) {
		return std::pair<std::string, std::string>();
	}
	
	// m_headの位置から検索を開始する
	for(unsigned i=m_head.second; i<m_formatted_data.size(); i++) {
		ResourceProperty rcp = m_formatted_data[i];

		if(rcp.task_id == task_id) {
			m_head.second = i+1;
			return std::pair<std::string, std::string>(rcp.resourcetype, rcp.filename);
		}
	}
	return std::pair<std::string, std::string>();
}


//------------------------------------------------------------------------------------------------------
//  Resourceの実装
//------------------------------------------------------------------------------------------------------

/*! @brief コンストラクタ
 読み込むcsvファイルを指定する
 param[in] csvFilename csvファイルのパス
 */
Resource::Resource(const std::string& csvFilename) {
	mp_rclr = new ResourceListReader(csvFilename.c_str());
}

/*! @brief デストラクタ
 保持している全てのリソースを開放する．ついでにResourceListReaderも開放する．
 */
Resource::~Resource() {
	releaseAll();
	delete mp_rclr;
}

/*! @brief リソースファイルをロードするクラス
 @param[in] ロードしたいタスクのID
 @return リソースのロードに成功した場合trueを返す
 失敗した場合はfalseを返す
 */
bool Resource::load(const std::string& task_id) {

	for(auto filedata = mp_rclr->search(task_id);
			filedata.first != "";
			filedata = mp_rclr->search(task_id)) {
		std::string resourcetype = filedata.first;
		std::string filename = filedata.second;
		std::string key = createKey(task_id, filename, resourcetype);

		assert(isEmpty(key) && "既にロードされているリソースをロードしようとしました");

		ResourceHandleFactory rhf;
		ResourceHandle* handle = rhf.createHandle(filename, resourcetype);
		if (handle->getHandle() == -1) { return false; }

		m_hash[key] = handle;
	}
	return true;
}

/*! @brief キーに対応したリソースを開放する
 param[in] key ハッシュテーブルのキー値
 return リソースの開放に成功した場合true, 失敗した場合falseを返す
 */
bool Resource::release(const std::string& key) {
	if (isEmpty(key)) { return false; }
	
	delete m_hash.at(key);
	m_hash.erase(key);

	return true;
}

/*! @brief タスクを検索し，対応するリソースを全て開放する
 param[in] task_id タスクに対応するid
 return 開放に成功した場合はtrueを返す
 */
bool Resource::releaseLump(const std::string& task_id) {

	for (auto filedata = mp_rclr->search(task_id);
		filedata.first != "";
		filedata = mp_rclr->search(task_id)) {

		std::string resourcetype = filedata.first;
		std::string filename = filedata.second;
		std::string key = createKey(task_id, filename, resourcetype);

		if (isEmpty(key)) {
			if (!release(key)){
				return false;
			}
		}
	}
	return true;
}

/*! @brief 登録された全てにリソースを開放する
 return 成功した場合はtrue, 失敗した場合はfalseを返す
 */
bool Resource::releaseAll() {
	ResourceHash::iterator it = m_hash.begin();
	while (it != m_hash.end()) {
		if (it->first != "") {
			delete it->second;
			m_hash.erase(it++);
		}
		else {
			it++;
		}
	}
	return true;
}

/*! @brief ロードしたリソースのハンドルを返す
 param[in] key リソースのキー値
 return int型のハンドルを返す．キー値に対応するハンドルが見つからない場合は-1を返す
 */
int Resource::getResource(const std::string& key) {
	if (isEmpty(key)) { return -1; }
	ResourceHandle* Handle = m_hash.at(key);
	return Handle->getHandle();
}

/*! @brief []演算子のオーバーロード，getResourceメソッドを同じ動きをする */
int Resource::operator[](const std::string& key) {
	return getResource(key);
}

/*! @brief task_id, resoucetype, filenameを指定してリソースをロードする
 @param[in] task_id タスクの識別子
 @param[in] resourcetype リソースの種類
 @param[in] filename ファイルの名前，パス
 @return 成功したらtrue 失敗したらfalseを返す
 */
bool Resource::setResource(const std::string& task_id,
	const std::string& resourcetype,
	const std::string& filename) {

	std::string key = createKey(task_id, filename, resourcetype);
	assert(isEmpty(key) && "既にロードされているリソースをロードしようとしました");

	ResourceHandleFactory rhf;
	ResourceHandle* handle = rhf.createHandle(filename, resourcetype);
	if (handle->getHandle() == -1) { return false; }

	m_hash[key] = handle;
	return true;
}

/*! @brief m_hashが空かどうかを返す
 @return trueなら空
 */
bool Resource::isEmpty() {
	return m_hash.empty();
}

/*! @brief key値でm_hashを検索し，値が空かどうかを返す
 @param[in] key m_hashのキー値
 @return trueなら空
 */
bool Resource::isEmpty(const std::string& key) {
	if (m_hash.count(key) == 0) { return true; }
	return false;
}

/*! @brief ファイルの名前と種類からハッシュテーブルのキー値を生成する
 @param[in] filepath ファイルのパス
 @param[in] resourcetype ファイルの種類
 @return キー値となるstring型の文字列
 */
std::string Resource::createKey(std::string task_id, std::string filepath, std::string resourcetype) {

	std::string pattern = "[a-zA-Z0-9\\-_]+";
	std::regex re(pattern);
	std::smatch match;

	std::string target = filepath;
	std::vector<std::string> matchlist;

	while (std::regex_search(target, match, re)) {
		for (auto submatch : match) {
			matchlist.push_back(match[0].str());	// ヒットした文字列を登録する
			target = match.suffix().str();	// ヒットしなかった文字列を次のtargetを設定する．
		}
	}

	if (matchlist.size() <= 1) { return nullptr; }

	matchlist.pop_back();	// 一番後ろに登録されるのは拡張子なので削除
	std::string filename = matchlist.back();	// ファイル名を取得
	matchlist.pop_back();

	// matchlist内の文字列から先頭2文字を:(コロン)でつなげてキー値にする
	std::string key = task_id + ":" + resourcetype + ":";
	const int prefix_size = 2;
	for (auto it = matchlist.begin(); it != matchlist.end(); it++) {
		key += it->substr(0, prefix_size);
		key += ":";
	}
	key += filename;

	return key;
}

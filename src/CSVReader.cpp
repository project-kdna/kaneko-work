/*! @file
 * @brief csvファイルからデータの読み出しを行うクラス
 * @author 金子
 * @date (2014/10/05)
 */

#include "CSVReader.h"

#include <iostream>
#include <fstream>
#include <sstream>

#include <string>
#include <vector>

/*! @brief CSVファイルを読みだして内容をメンバに登録するメソッド
 @param[in] filename ファイル名
 @return 成功した場合はtrueを返す．失敗した場合はfalseを返す．
 */
bool CSVReader::load(const char* filename){

	std::ifstream ifs(filename, std::ios::in); // ファイルストリームを開く
	if (ifs.fail()) {
		std::cerr << "could not file open." << std::endl;
		return false;
	}

	// 1行ずつ読み込みm_dataに挿入する
	std::string line;
	int row = m_row = 0;
	m_col = 0;
	m_line = 0;
	while (getline(ifs, line)) {
		m_data.push_back(line);	// 一行ずつ m_data に挿入

		// 列数を数える
		std::istringstream iss(line);
		std::string cell;
		int col = 0;
		while (getline(iss, cell, ',')) {
			col++;
		}
		if (col > m_col){ m_col = col; }	// 列数は最大値を登録する

		row++;	// 行数を数える (こちらも最大値)
	}
	m_row = row;

	ifs.close();	// ファイルストリームを閉じる

	return true;
}

/*! @brief 読み込んだCSVのデータを一行ずつ読み出す
 @return m_line行目のデータを返す．CSVの各セルを文字列として，1行分のデータをvectorにまとめている
 最終行まで読みだした場合，空行の場合はsizeが0のvectorを返す.
 */
std::vector<std::string> CSVReader::readLine(){
	return readLine(m_line);
}

/*! @brief 読み込んだCSVのデータを一行ずつ読み出す
 @param[in] line 読み出したい行の行数．(0行目〜最終行目)　読みだしたあと，m_line=line+1に変更する
 @return m_line行目のデータを返す．CSVの各セルを文字列として，1行分のデータをvectorにまとめている
 最終行以降を読み出そうとした場合，指定の行にデータがない場合はsizeが0のvectorを返す
 */
std::vector<std::string> CSVReader::readLine(const int line){

	std::vector<std::string> formatted_line(0);

	if (line >= m_row){ return formatted_line; }

	std::string str_line = m_data[m_line];
	std::istringstream iss(str_line);
	std::string cell;
	while (getline(iss, cell, ',')){		// 指定した行を , で分割する
		trim(cell);						// 空白，タブは取り除く
		formatted_line.push_back(cell);
	}
	m_line = line + 1;	// 次に読み出す行をline+1とする
	return formatted_line;
}

/*! @brief 文字列に含まれる空白とタブを取り除く
 @param[in,out] &buf 空白とタブを取り除きたい文字列
 */
void CSVReader::trim(std::string &buf)
{
	size_t pos;
	while ((pos = buf.find_first_of(" 　\t")) != std::string::npos){
		buf.erase(pos, 1);
	}
}

#ifndef CSVREADER_PROJECTKDNA_H_
#define CSVREADER_PROJECTKDNA_H_

#include <string>
#include <vector>

class CSVReader{
public:
	bool load(const char* filename);
	std::vector<std::string> readLine();
	std::vector<std::string> readLine(const int line);	// 行数を指定して読み出し

	// getter
	int getRow(){ return m_row; }
	int getColumn(){ return m_col; }
	int getLine(){ return m_line; }

private:
	int m_row;		//!< 行数
	int m_col;		//!< 列数
	int m_line;		//!< 次に読み出す行数
	std::vector<std::string> m_data;	//!< 読み込んだCSVデータ

	void trim(std::string &buf);
};

#endif CSVREADER_PROJECTKDNA_H_
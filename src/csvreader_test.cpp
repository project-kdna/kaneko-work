#include "CSVReader.h"

#include <iostream>
#include <vector>
#include <string>

static void testCSV()
{
	CSVReader csvreader;
	bool isLoad = csvreader.load("data/sound_list.csv");

	while(true){
		std::vector<std::string> col = csvreader.readLine();
		if(col.size() == 0){ break; }

		std::vector<std::string>::iterator it;
		for(it=col.begin(); it!=col.end(); it++){
			std::cout << *it << " ";
		}
		std::cout << std::endl;
	}
	return;
}

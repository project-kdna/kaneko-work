#include "DxLib.h"

#include "csvreader_test.cpp"
#include "resource_test.cpp"

#include <iostream>

// メモリリーク調査
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#define new ::new(_NORMAL_BLOCK, __FILE__, __LINE__)

// プログラムは WinMain から始まります
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{

	// コンソール呼び出し
	AllocConsole();
	FILE* fp;
	freopen_s(&fp, "CONOUT$", "w", stdout);
	freopen_s(&fp, "CONIN$", "r", stdin);

	if (DxLib_Init() == -1)		// ＤＸライブラリ初期化処理
	{
		return -1;			// エラーが起きたら直ちに終了
	}
	
	ChangeWindowMode(TRUE);

	testResource();
	testImageDraw();
	testSoundPlay();
	WaitKey();

	DxLib_End();				// ＤＸライブラリ使用の終了処理

	FreeConsole();		// コンソールの開放

	return 0;				// ソフトの終了 
}

#ifndef RESOURCE_PROJECTKDNA_H_
#define RESOURCE_PROJECTKDNA_H_


#include <string>
#include <unordered_map>

namespace kdna {

namespace {

class ResourceHandle {
public:
	// constructor, destructor
	ResourceHandle(int handle){ setHandle(handle); }
	virtual ~ResourceHandle(){}

	// getter
	virtual int getHandle(){ return m_handle; }

protected:
	// setter
	virtual void setHandle(int handle){ m_handle = handle; }

private:
	// var
	int m_handle;
};

class ImageHandle : public ResourceHandle {
public:
	// destructor
	ImageHandle(int handle) : ResourceHandle(handle){}
	~ImageHandle();

	// getter, setter
	int getHandle(){ return Super::getHandle(); }
	void setHandle(int handle){ Super::setHandle(handle); }

private:
	typedef ResourceHandle Super;
};

class BGMHandle : public ResourceHandle {
public:
	// constructor, destructor
	BGMHandle(int handle) : ResourceHandle(handle){}
	~BGMHandle();

	// getter, setter
	int getHandle(){ return Super::getHandle(); }
	void setHandle(int handle){ Super::setHandle(handle); }

private:
	typedef ResourceHandle Super;
};

class SEHandle : public ResourceHandle {
public:
	// constructor, destructor
	SEHandle(int handle) : ResourceHandle(handle){}
	~SEHandle();

	// getter, setter
	int getHandle(){ return Super::getHandle(); }
	void setHandle(int handle){ Super::setHandle(handle); }

private:
	typedef ResourceHandle Super;
};

class ResourceHandleFactory {
public:
	ResourceHandle* createHandle(std::string filename, std::string resourcetype);
};

class ResourceListReader {
public:
	// constructor
	ResourceListReader(const char* filename);

	// method
	std::pair<std::string, std::string> search(std::string task_id);

private:
	struct ResourceProperty {
		std::string task_id;
		std::string resourcetype;
		std::string filename;
	};

	std::vector<ResourceProperty> m_formatted_data;
	std::pair<std::string, int> m_head;
};
}	// namespace anonymous

typedef std::unordered_map<std::string, ResourceHandle*> ResourceHash;	//!< ResourceHandleを登録するmap

class Resource {
public:
	// constructor, destructor
	Resource(const std::string& csvFilename);	// 読み込むcsvファイルを指定する
	~Resource();		// m_hashに登録している全てのリソースを開放する

	// method
	bool load(const std::string& task_id);	// csvファイルを検索し対象となる全てのリソースをロードする
	bool release(const std::string& key);	// キーに対応したリソースを開放する
	bool releaseLump(const std::string& task_id);	// csvファイルを検索し対象となる全てのリソースを開放する
	bool releaseAll();							// ロードしたリソースを全て開放する

	int getResource(const std::string& key);		// キーに対応したリソースのハンドルを返す
	int operator[](const std::string& key);		// []演算子でgetResourceの動作を行う
	bool setResource(const std::string& task_id, const std::string& resoucetype, const std::string& filename);	// リソースファイルを指定してロードする

	bool isEmpty();
	bool isEmpty(const std::string& key);

private:
	ResourceListReader* mp_rclr;
	ResourceHash m_hash;

	// method
	std::string createKey(std::string task_id, std::string filepath, std::string resourcetype);
};

} // namespace kdna

#endif // !RESOURCE_PROJECTKDNA_H_
